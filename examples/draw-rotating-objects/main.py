import numpy as np
from vispy import app, gloo
from vispy.util.transforms import translate

from gloo_scene_renderer import Scene, MVPObject
from gloo_scene_renderer.custom_objects import Text


def cube():
    """
    Build vertices for a colored cube.

    V  is the vertices
    I1 is the indices for a filled cube (use with GL_TRIANGLES)
    I2 is the indices for an outline cube (use with GL_LINES)
    """
    vtype = [("a_position", np.float32, 3), ("a_normal", np.float32, 3), ("a_color", np.float32, 4)]
    # Vertices positions
    v = [[1, 1, 1], [-1, 1, 1], [-1, -1, 1], [1, -1, 1], [1, -1, -1], [1, 1, -1], [-1, 1, -1], [-1, -1, -1]]
    # Face Normals
    n = [[0, 0, 1], [1, 0, 0], [0, 1, 0], [-1, 0, 0], [0, -1, 0], [0, 0, -1]]
    # Vertices colors
    c = [[0, 1, 1, 1], [0, 0, 1, 1], [0, 0, 0, 1], [0, 1, 0, 1], [1, 1, 0, 1], [1, 1, 1, 1], [1, 0, 1, 1], [1, 0, 0, 1]]

    V = np.array(
        [
            (v[0], n[0], c[0]),
            (v[1], n[0], c[1]),
            (v[2], n[0], c[2]),
            (v[3], n[0], c[3]),
            (v[0], n[1], c[0]),
            (v[3], n[1], c[3]),
            (v[4], n[1], c[4]),
            (v[5], n[1], c[5]),
            (v[0], n[2], c[0]),
            (v[5], n[2], c[5]),
            (v[6], n[2], c[6]),
            (v[1], n[2], c[1]),
            (v[1], n[3], c[1]),
            (v[6], n[3], c[6]),
            (v[7], n[3], c[7]),
            (v[2], n[3], c[2]),
            (v[7], n[4], c[7]),
            (v[4], n[4], c[4]),
            (v[3], n[4], c[3]),
            (v[2], n[4], c[2]),
            (v[4], n[5], c[4]),
            (v[7], n[5], c[7]),
            (v[6], n[5], c[6]),
            (v[5], n[5], c[5]),
        ],
        dtype=vtype,
    )
    I1 = np.resize(np.array([0, 1, 2, 0, 2, 3], dtype=np.uint32), 6 * (2 * 3))
    I1 += np.repeat(4 * np.arange(2 * 3, dtype=np.uint32), 6)

    I2 = np.resize(np.array([0, 1, 1, 2, 2, 3, 3, 0], dtype=np.uint32), 6 * (2 * 4))
    I2 += np.repeat(4 * np.arange(6, dtype=np.uint32), 8)

    return V, I1, I2


def triangle():
    v = [[-1.75, -1, 1], [-1.25, -1, 1], [-1.5, 0, 1]]
    n = [[0, 0, 0]]
    c = [[0, 0, 0, 1]]
    vtype = [("a_position", np.float32, 3), ("a_normal", np.float32, 3), ("a_color", np.float32, 4)]
    V = np.array([(v[0], n[0], c[0]), (v[1], n[0], c[0]), (v[2], n[0], c[0])], dtype=vtype)
    I = np.array([0, 1, 2], dtype=np.uint32)
    return V, I


def init_callback(scene):
    scene.x = 0
    scene.y = 0

    # scene.programs["default"]["u_projection"] = np.eye(4, dtype=np.float32)
    scene.programs["default"]["u_model"] = np.eye(4, dtype=np.float32)
    scene.programs["default"]["u_view"] = translate((scene.x, scene.y, -5))

    scene.objects["cube1"].theta = 0
    scene.objects["cube1"].phi = 0
    scene.objects["cube1"].translate(0.5, 0.5, 1.5)
    scene.objects["cube1"].scale(-0.5)
    scene.objects["cube2"].blend = False
    scene.objects["cube2"].polygon_offset_fill = False
    scene.objects["cube2"].draw_type = "lines"
    scene.objects["cube2"].u_color = (0, 0, 0, 1)
    scene.objects["cube3"].translate(-1.5, -1.5, 0)
    scene.objects["cube3"].scale(-0.5)


def draw_callback(scene, event):
    if scene.key_is_pressed["A"]:
        scene.x += 0.1
    if scene.key_is_pressed["D"]:
        scene.x -= 0.1
    if scene.key_is_pressed["W"]:
        scene.y -= 0.1
    if scene.key_is_pressed["S"]:
        scene.y += 0.1
    if scene.key_is_pressed["Up"]:
        scene.objects["cube3"].translate_delta(0, -0.1, 0)
    if scene.key_is_pressed["Down"]:
        scene.objects["cube3"].translate_delta(0, 0.1, 0)
    if scene.key_is_pressed["Left"]:
        scene.objects["cube3"].translate_delta(0.1, 0, 0)
    if scene.key_is_pressed["Right"]:
        scene.objects["cube3"].translate_delta(-0.1, 0, 0)
    if scene.key_is_pressed["R"]:
        init_callback(scene)

    if scene.key_is_pressed["G"]:
        scene.objects["text_hello"].translate(-0.05, 0)
    if scene.key_is_pressed["H"]:
        scene.objects["text_hello"].translate(0.05, 0)

    gloo.clear()
    # Update camera
    scene.programs["default"]["u_model"] = np.eye(4, dtype=np.float32)
    scene.programs["default"]["u_view"] = translate((scene.x, scene.y, -5))

    # Updates for objects
    scene.objects["cube1"].theta += 0.5
    scene.objects["cube1"].phi += 0.5
    scene.objects["cube1"].rotate(0, scene.objects["cube1"].theta, scene.objects["cube1"].phi)

    scene.draw()


def main():
    V, I1, I2 = cube()
    cube1 = MVPObject("cube1", V, I1, "default")
    cube2 = MVPObject("cube2", V, I2, "default")
    triangle1 = MVPObject("triangle1", *triangle(), "default")

    objects = [cube1, cube2, triangle1, cube1.clone("cube3"), Text("hello", position=(0.5, 0.5))]

    canvas = Scene(objects=objects, init_callback=init_callback, draw_callback=draw_callback)
    canvas.show()
    app.run()


if __name__ == "__main__":
    main()
