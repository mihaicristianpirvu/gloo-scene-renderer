from __future__ import annotations
from typing import List, Callable, Dict
import numpy as np
from vispy.app import Canvas, Timer
from vispy import gloo
from vispy.util.transforms import perspective, translate

from .logger import logger
from .scene_objects import SceneObject
from .shaders import get_default_program


def _default_init_callback(scene):
    scene.programs["default"]["u_model"] = np.eye(4, dtype=np.float32)
    scene.programs["default"]["u_view"] = translate((0, 0, -5))


def _default_draw_callback(scene, event):
    scene.draw()


class Scene(Canvas):
    def __init__(
        self,
        objects: List[SceneObject],
        programs: Dict[str, gloo.Program] = None,
        init_callback: Callable[[Scene]] = None,
        draw_callback: Callable[[Scene]] = None,
        window_size=(800, 600),
    ):
        super().__init__(keys="interactive", size=window_size)
        if programs is None:
            programs = {}
        if not "default" in programs:
            logger.debug("Default shader program was not provided, using a default MVP shader.")
            programs["default"] = get_default_program()
        if init_callback is None:
            logger.info("No init callback provided, using a default one")
            init_callback = _default_init_callback
        if draw_callback is None:
            logger.info("No draw callback provided, using a default one")
            draw_callback = _default_draw_callback
        if objects is None or len(objects) == 0:
            logger.info("No objects were provided for this scene.")

        self.objects = {obj.name: obj for obj in objects}
        self.programs = programs
        self.init_callback = init_callback
        self.draw_callback = draw_callback
        self._timer = Timer("auto", start=True)
        self.key_is_pressed = self._init_key_press()

        gloo.set_clear_color("white")
        gloo.set_state("opaque")
        gloo.set_polygon_offset(1, 1)

        self.init_callback(self)

    def _init_key_press(self):
        key_is_pressed = {chr(x): False for x in range(ord("A"), ord("Z") + 1)}
        key_is_pressed = {**key_is_pressed, "Right": False, "Left": False, "Up": False, "Down": False}
        return key_is_pressed

    def on_resize(self, event):
        gloo.set_viewport(0, 0, event.physical_size[0], event.physical_size[1])
        self.projection = perspective(45.0, event.size[0] / float(event.size[1]), 2.0, 10.0)
        self.programs["default"]["u_projection"] = self.projection

    def on_draw(self, event):
        self.draw_callback(self, event)

    def on_key_press(self, event):
        if event.key is None:
            return
        self.key_is_pressed[event.key.name] = True

    def on_key_release(self, event):
        if event.key is None:
            return
        self.key_is_pressed[event.key.name] = False

    def draw(self):
        for _, object in self.objects.items():
            program = None
            if hasattr(object, "program_name"):
                program = self.programs[object.program_name]
                program.bind(object.vertices)
            object.draw(self, program=program)

        self.update()
