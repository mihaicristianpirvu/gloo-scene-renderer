from vispy.visuals import TextVisual

from ..scene_objects import SceneObject
from ..logger import logger

class Text(SceneObject):
    def __init__(self, text, position, name: str = None):
        assert len(position) == 2 and 0 <= position[0] <= 1 and 0 <= position[1] <= 1
        self._text = TextVisual(text=text)
        if name is None:
            name=f"text_{self._text.text}"
            logger.debug(f"Text object has no name. Setting name: '{name}'")
        super().__init__(name=name)
        self.current_vp = None
        self.position = position

    def draw(self, scene, **kwargs):
        vp = (0, 0, scene.physical_size[0], scene.physical_size[1])
        if self.current_vp is None or (self.current_vp is not None and self.current_vp != vp):
            self.current_vp = vp
            self._text.transforms.configure(canvas=scene, viewport=vp)
            self._text.update()

        current_pos = [[self.position[0] * scene.physical_size[0], self.position[1] * scene.physical_size[1], 0]]
        if (self._text.pos == current_pos).sum() != 0:
            self._text.pos = current_pos
            self._text.update()

        self._text.draw()

    def translate(self, delta_x: float, delta_y: float):
        new_x = self.position[0] + delta_x
        new_y = self.position[1] + delta_y
        if not (0 <= new_x <= 1) or not (0 <= new_y <= 1):
            logger.info(f"Cannot transalte text '{self.name}' at pos '{self.position}' with delta"
                        f"'{delta_x}, {delta_y}'")
            return
        self.position = (new_x, new_y)
