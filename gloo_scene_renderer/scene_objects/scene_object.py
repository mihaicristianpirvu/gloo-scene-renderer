from abc import ABC, abstractmethod


class SceneObject(ABC):
    def __init__(self, name: str):
        self.name = name

    @abstractmethod
    def draw(self, scene, **kwargs):
        """Main method of a scene object"""
