from typing import Union, Tuple
import numpy as np
from vispy import gloo

from .scene_object import SceneObject

_GLOBAL_OBJECTS = {}


class ShaderObject(SceneObject):
    def __init__(
        self,
        name: str,
        vertices: Union[np.ndarray, gloo.VertexBuffer],
        indices: Union[np.ndarray, gloo.IndexBuffer],
        program_name: str = "default",
        draw_type: str = "triangles",
        blend: bool = False,
        depth_test: bool = True,
        polygon_offset_fill: bool = True,
        u_color: Tuple[float, float, float, float] = (1, 1, 1, 1),
    ):
        if id(vertices) not in _GLOBAL_OBJECTS:
            gloo_buffer = vertices if isinstance(vertices, gloo.VertexBuffer) else gloo.VertexBuffer(vertices)
            _GLOBAL_OBJECTS[id(vertices)] = gloo_buffer

        if id(indices) not in _GLOBAL_OBJECTS:
            gloo_buffer = indices if isinstance(indices, gloo.IndexBuffer) else gloo.IndexBuffer(indices)
            _GLOBAL_OBJECTS[id(indices)] = gloo_buffer

        self.name = name

        self.program_name = program_name
        self.vertices = _GLOBAL_OBJECTS[id(vertices)]
        self.indices = _GLOBAL_OBJECTS[id(indices)]
        self.draw_type = draw_type
        self.blend = blend
        self.depth_test = depth_test
        self.polygon_offset_fill = polygon_offset_fill
        self.u_color = u_color

    def draw(self, scene, program: gloo.Program):
        """Draw the current object. Override this method for more advanced drawing"""
        gloo.set_state(blend=self.blend, depth_test=self.depth_test, polygon_offset_fill=self.polygon_offset_fill)
        program["u_color"] = self.u_color
        program.draw(self.draw_type, self.indices)

    def clone(self, new_name):
        """Clones the current object with same vertices and indices"""
        return type(self)(
            new_name,
            self.vertices,
            self.indices,
            self.program_name,
            self.draw_type,
            self.blend,
            self.depth_test,
            self.polygon_offset_fill,
        )
