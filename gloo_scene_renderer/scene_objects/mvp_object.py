import numpy as np
from vispy import gloo
from vispy.util.transforms import translate, rotate, scale
from .shader_object import ShaderObject
from loguru import logger


class MVPObject(ShaderObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._u_model = None
        self.reset_position()

    @property
    def u_model(self) -> np.ndarray:
        """Simple model matrix given by translation * rotation * scaling."""
        rotation = rotate(self.rx, (1, 0, 0)) @ rotate(self.ry, (0, 1, 0)) @ rotate(self.rz, (0, 0, 1))
        translation = translate([self.tx, self.ty, self.tz])
        scaling = scale([self.s, self.s, self.s])
        res = (translation @ rotation @ scaling).astype(np.float32)
        return res

    def reset_position(self):
        """Resets the object to the default position as given by vertices"""
        self.tx, self.ty, self.tz = 0, 0, 0
        self.rx, self.ry, self.rz = 0, 0, 0
        self.s = 1

    def translate(self, tx: float, ty: float, tz: float):
        """Translates the current position with delta tx, ty, tz"""
        self.tx = tx
        self.ty = ty
        self.tz = tz

    def rotate(self, rx: float, ry: float, rz: float):
        """Rotates the current position with delta rx, ry, rz"""
        self.rx = rx
        self.ry = ry
        self.rz = rz

    def scale(self, scale: float):
        self.s = scale

    def translate_delta(self, dx: float, dy: float, dz: float):
        self.translate(self.tx + dx, self.ty + dy, self.tz + dz)

    def rotate_delta(self, dx: float, dy: float, dz: float):
        self.rotate(self.rx + dx, self.ry + dy, self.rz + dz)

    def scale_delta(self, delta: float):
        self.scalte(self.scale + delta)

    def draw(self, scene, program: gloo.Program):
        program["u_model"] = self.u_model
        super().draw(scene, program=program)

    def __str__(self):
        f_str = f"Scene object {self.name}. Position: {self.tx}, {self.ty}, {self.tx}"
        return f_str
